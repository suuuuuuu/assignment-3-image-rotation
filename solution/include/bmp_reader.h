#ifndef BMP_READER_H
#define BMP_READER_H

#include "image.h"
#include <stdio.h>

enum read_status {
    READ_OK,
    READ_FATAL_ERROR,
    READ_END_OF_FILE_NOT_EXPECTED,
    READ_BAD_RESOLUTION_IN_HEADER,
    READ_UNSUPPORTED_COLOR_PALETE
};

enum read_status read_image_from_bmp(FILE* bmp_file_input, Image* image_result);

#endif
