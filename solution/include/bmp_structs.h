#ifndef BMP_HEADERS_H
#define BMP_HEADERS_H

#include "image.h"
#include  <stdint.h>

struct __attribute__((packed))  bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

#define DEFAULT_BMP_FILE_TYPE 0x4D42
#define COLOR_SIZE 24
#define DEFAULT_X_PELS_PER_METER 2834
#define DEFAULT_Y_PELS_PER_METER 2834

struct __attribute__((packed))  bmp_pixel
{
    uint8_t blue;
    uint8_t green;
    uint8_t red;
};

struct bmp_header init_empty_bmp_header(void);

Pixel bmp_pixel_to_pixel(struct bmp_pixel bmp_pixel);

struct bmp_pixel pixel_to_bmp_pixel(Pixel pixel);

void print_header(struct bmp_header);

size_t get_bmp_24_padding(uint32_t width);

struct bmp_header init_40b_bmp_header(uint32_t height, uint32_t width);

struct bmp_pixel** init_empty_bmp_pixel_map(int64_t height, int64_t width);

void free_bmp_pixel_map(struct bmp_pixel** data, size_t height);


#endif
