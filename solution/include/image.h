#ifndef IMAGE_H 
#define IMAGE_H

#include <stddef.h>
#include <stdint.h>

typedef struct image {
  size_t width, height;
  struct pixel** data;
} Image;

// представляется в формате RGB
typedef struct pixel { 
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} Pixel;

void free_image(Image* image);

Image init_image(uint64_t height, uint64_t width);

Image init_empty_image(void);

Pixel** init_empty_pixel_map(uint64_t height, uint64_t width);

void free_pixel_map(Pixel** data, size_t height);

#endif
