#ifndef BMP_WRITER_H
#define BMP_WRITER_H

#include "image.h"
#include <stdio.h>

enum  write_status  {
    WRITE_OK,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum write_status write_image_to_bmp(FILE* bmp_file_output, const Image* image);

#endif
