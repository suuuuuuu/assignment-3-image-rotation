#ifndef FILE_ACCESS_CHECKS_H
#define FILE_ACCESS_CHECKS_H

#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

bool exists_file_check(const char* path);

bool read_access_file_check(const char* path);

bool write_access_file_check(const char* path);

FILE* open_binary_readonly(const char* path);

FILE* create_if_hadnt_and_open_binary_writeonly(const char* path);

#endif
