#ifndef IMAGE_TRANSFORM_H 
#define IMAGE_TRANSFORM_H

#include "image.h"

Image rotate_image(Image* image_to_rotate);

#endif
