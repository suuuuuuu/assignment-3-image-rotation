#include "../include/image.h"
#include "../include/bmp_reader.h"
#include "../include/bmp_structs.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static enum read_status read_header_from_bmp(FILE* bmp_file_input, struct bmp_header* bmp_header);
static enum read_status read_next_pixels_row(struct bmp_pixel* row_array, const size_t WIDTH, const size_t END_PADDING, FILE* bmp_input_file);
static void bmp_pixels_to_pixels(struct bmp_pixel** bmp_pixels, Pixel** pixels, const size_t height, const size_t width);
static enum read_status get_stream_error_for_fread(FILE* stream);
static enum read_status validate_header(struct bmp_header* bmp_header);

enum read_status read_image_from_bmp(FILE* bmp_file_input, Image* image_result) {
    struct bmp_header bmp_header = init_empty_bmp_header(); 
    enum read_status header_read_res = read_header_from_bmp(bmp_file_input, &bmp_header);
    if (header_read_res != READ_OK) {
        return header_read_res;
    }
    enum read_status header_validate_res = validate_header(&bmp_header);
    if (header_validate_res != READ_OK) {
        return header_validate_res;
    }
    const int64_t HEIGHT = bmp_header.biHeight;
    const int64_t WIDTH = bmp_header.biWidth;
    struct bmp_pixel** bmp_pixels = init_empty_bmp_pixel_map(HEIGHT, WIDTH);
    const size_t END_PADDING = get_bmp_24_padding(WIDTH);
    for (size_t h = 0; h < HEIGHT; h++) {
        enum read_status row_read_status = read_next_pixels_row(bmp_pixels[h], WIDTH, END_PADDING, bmp_file_input);
        if (row_read_status != READ_OK) {
            return row_read_status;
        }
    }
    *image_result = init_image(HEIGHT, WIDTH);
    bmp_pixels_to_pixels(bmp_pixels, image_result->data, HEIGHT, WIDTH);
    free_bmp_pixel_map(bmp_pixels, HEIGHT);
    return READ_OK; // change
}

static enum read_status validate_header(struct bmp_header* bmp_header) {
    if ((bmp_header->biWidth == 0 && bmp_header->biHeight != 0) ||
            (bmp_header->biHeight == 0 && bmp_header->biWidth != 0)) {
        return READ_BAD_RESOLUTION_IN_HEADER;
    }
    if (bmp_header->biWidth < 0 || bmp_header->biHeight < 0) {
        return READ_BAD_RESOLUTION_IN_HEADER;
    }
    if (bmp_header->biBitCount != 24) {
        return READ_UNSUPPORTED_COLOR_PALETE;
    }
    return READ_OK;
}

static enum read_status read_header_from_bmp(FILE* bmp_file_input, struct bmp_header* bmp_header) {
    unsigned long header_read_result_code = fread(bmp_header, sizeof(struct bmp_header), 1, bmp_file_input); 
    if (header_read_result_code != 1) {
        return get_stream_error_for_fread(bmp_file_input);
    }
    return READ_OK;
}

static enum read_status read_next_pixels_row(struct bmp_pixel* row_array, const size_t WIDTH, const size_t END_PADDING, FILE* bmp_input_file) {
    unsigned long row_read_count = fread(row_array, sizeof(struct bmp_pixel), WIDTH, bmp_input_file);
    if (row_read_count != WIDTH) {
        return get_stream_error_for_fread(bmp_input_file);
    }
    size_t skip_padding_res = fseek(bmp_input_file, (long) END_PADDING, SEEK_CUR); 
    if (skip_padding_res != 0) {
        return READ_FATAL_ERROR;
    }
    return READ_OK;
}

static enum read_status get_stream_error_for_fread(FILE* stream) {
    if (feof(stream)) {
        return READ_END_OF_FILE_NOT_EXPECTED;
    } else {
        return READ_FATAL_ERROR;
    }
}

static void bmp_pixels_to_pixels(struct bmp_pixel** bmp_pixels, Pixel** pixels, const size_t HEIGHT, const size_t WIDTH) {
    for (size_t h = 0; h < HEIGHT; h++) {
        for (size_t w = 0; w < WIDTH; w++) {
            pixels[h][w] = bmp_pixel_to_pixel(bmp_pixels[h][w]);
        }
    }
}

