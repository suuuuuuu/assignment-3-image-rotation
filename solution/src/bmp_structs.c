#include "../include/bmp_structs.h"
#include <malloc.h>
#include <stdint.h>

struct bmp_header init_empty_bmp_header(void) {
    return (struct bmp_header) {
        .bfType=0
    };
}


struct bmp_header init_40b_bmp_header(uint32_t height, uint32_t width) {
    uint32_t header_size = 40;
    uint32_t off_bits = header_size + 14;
    uint32_t image_row_size = width * sizeof(struct bmp_pixel) + get_bmp_24_padding(width);
    uint32_t image_size = image_row_size * height;
    return (struct bmp_header) {
        .bfType = DEFAULT_BMP_FILE_TYPE,
        .bfileSize = off_bits + image_size,
        .bfReserved = 0,
        .bOffBits = off_bits,
        .biSize = header_size,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = 1,
        .biBitCount = COLOR_SIZE,
        .biCompression = 0,
        .biSizeImage = image_size, 
        .biXPelsPerMeter = DEFAULT_X_PELS_PER_METER,
        .biYPelsPerMeter = DEFAULT_Y_PELS_PER_METER,
        .biClrUsed = 0,
        .biClrImportant = 0 
    };
}

Pixel bmp_pixel_to_pixel(struct bmp_pixel bmp_pixel) {
    return (Pixel) {
        .red=bmp_pixel.red,
        .green=bmp_pixel.green,
        .blue=bmp_pixel.blue
    };
}

struct bmp_pixel pixel_to_bmp_pixel(Pixel pixel) {
    return (struct bmp_pixel) {
        .red=pixel.red,
        .green=pixel.green,
        .blue=pixel.blue
    };
}

size_t get_bmp_24_padding(uint32_t width) {
    size_t pixel_size = sizeof(struct bmp_pixel);
    size_t p = (width * pixel_size) % 4;
    if (p == 0) {
        return 0;
    }
    return ((size_t) 4) - p;
}

struct bmp_pixel** init_empty_bmp_pixel_map(int64_t height, int64_t width) {
    struct bmp_pixel** pixels = calloc(height, sizeof(struct bmp_pixel*));
    for (size_t h = 0; h < height; h++) {
        pixels[h] = calloc(width, sizeof(struct bmp_pixel));
    }
    return pixels;
}

void free_bmp_pixel_map(struct bmp_pixel** data, size_t height) {
    for (size_t h = 0; h < height; h++) {
        free(data[h]);
    }
    free(data);
}
