#include "../include/image.h"
#include "../include/image_transform.h"

Image rotate_image(Image* image_to_rotate) {
    Image rotated_image = init_image(image_to_rotate->width, image_to_rotate->height);
    for (size_t w = 0; w < image_to_rotate->width; w++) {
        for (size_t h = 0; h < image_to_rotate->height; h++) {
            rotated_image.data[w][image_to_rotate->height - h - 1] = image_to_rotate->data[h][w];
        }
    }
    return rotated_image;
}
