#include <stdio.h>
#include <unistd.h>

// добавить -I в make
#include "../include/file_access.h"
#include "../include/bmp_reader.h"
#include "../include/bmp_writer.h"
#include "../include/image.h"
#include "../include/image_transform.h"

static void print_wrong_args_count_msg(const int args_count);
static void print_input_file_existance_check_failed_msg(char* in_file_path);
static void print_input_file_read_access_failed_msg(char* in_file_path);
static void print_output_file_exists_write_access_failed_msg(char* out_file_path);
static void print_bad_read_status_msg(enum read_status read_status);
static void print_bad_write_status_msg(enum write_status write_status);
static void print_rotated_write_success_msg(void);


int main( int argc, char** argv ) {
    if (argc != 3) {
        print_wrong_args_count_msg(argc);
        return 1;
    }
    char* in_file_path = argv[1];
    char* out_file_path = argv[2];
    if (!exists_file_check(in_file_path)) {
        print_input_file_existance_check_failed_msg(in_file_path);
        return 1;
    }
    if (!read_access_file_check(in_file_path)) {
        print_input_file_read_access_failed_msg(in_file_path);
        return 1;
    }
    if (exists_file_check(out_file_path) && !write_access_file_check(out_file_path)) {
        print_output_file_exists_write_access_failed_msg(out_file_path);
        return 1;
    }
    FILE* in_file = open_binary_readonly(in_file_path);
    Image read_image = init_empty_image(); 
    enum read_status image_read_status = read_image_from_bmp(in_file, &read_image);
    fclose(in_file);
    if (image_read_status != READ_OK) {
        print_bad_read_status_msg(image_read_status);
        return 1;
    }
    Image rotated_image = rotate_image(&read_image);
    free_image(&read_image);
    FILE* out_file = create_if_hadnt_and_open_binary_writeonly(out_file_path);
    enum write_status image_write_status = write_image_to_bmp(out_file, &rotated_image);
    fclose(out_file);
    free_image(&rotated_image);
    if (image_write_status != WRITE_OK) {
        print_bad_write_status_msg(image_write_status);
        return 1;
    }
    print_rotated_write_success_msg();
    return 0;
}

static void print_wrong_args_count_msg(const int args_count) {
    printf("Первым должен быть путь файла для переворота.\n"
        "Вторым - путь до выходного файла.\n"
        "Вы же указали %d аргумента(-ов) вместо требуемых двух.",
        args_count - 1);
}

static void print_input_file_existance_check_failed_msg(char* in_file_path) {
    printf("Путь файла для переворота (1-ый аргумент) неверный.\n"
        "Введенный путь: %s",
        in_file_path);
}

static void print_input_file_read_access_failed_msg(char* in_file_path) {
    printf("Файл для переворота (1-ый аргумент) запрещает доступ на чтение.\n"
        "Его путь: %s",
        in_file_path);
}

static void print_output_file_exists_write_access_failed_msg(char* out_file_path) {
    printf("Выходной файл (2-ой аргумент) существует, но запрещает доступ на запись.\n"
        "Перенесите файл куда-нибудь, если он важен, или просто удалите его.\n"
        "Его путь: %s",
        out_file_path);
}

static void print_bad_read_status_msg(enum read_status read_status) {
    switch (read_status) {
        case READ_END_OF_FILE_NOT_EXPECTED: 
            printf("При чтении часть файла ожидалась, но ее не оказалось на месте - файл закончился");
            break;
        case READ_FATAL_ERROR:
            printf("Фатальная ошибка чтения");
            break;
        case READ_BAD_RESOLUTION_IN_HEADER:
            printf("В хедере неверное разрешение картинки");
            break;
        case READ_UNSUPPORTED_COLOR_PALETE:
            printf("Программа не поддерживает глубины цвета, отличные от 24 бит");
            break;
        default:
            break;
    }
}

static void print_bad_write_status_msg(enum write_status write_status) {
    switch (write_status) {
        case WRITE_ERROR:
            printf("Ошибка записи в файл");
            break;
        default:
            break;
    }
}

static void print_rotated_write_success_msg(void) {
    printf("Картинка перевернута и успешно записана в выходной файл");
}
