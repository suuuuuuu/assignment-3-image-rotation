#include "../include/image.h"
#include <malloc.h>
#include <stdint.h>
#include <stdlib.h>

void free_image(Image* image) {
    free_pixel_map(image->data, image->height);
}

Image init_image(uint64_t height, uint64_t width) {
    return (Image) {
        .height=height,
        .width=width,
        .data=init_empty_pixel_map(height, width)
    };
}

Image init_empty_image(void) {
    return (Image) {
        .width=0
    };
}

Pixel** init_empty_pixel_map(uint64_t height, uint64_t width) {
    Pixel** data = calloc(sizeof(Pixel*), height);
    for (size_t h = 0; h < height; h++) {
        data[h] = calloc(sizeof(Pixel), width);
    }
    return data;
}

void free_pixel_map(Pixel** data, size_t height) {
    for (size_t h = 0; h < height; h++) {
        free(data[h]);
    }
    free(data);
}
